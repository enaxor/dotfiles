#!/bin/sh
today=$(date +%e)
cal=`cal -m | sed "s/$today/<b>$today<\/b>/g"`
notify-send -t 30000 "$cal" &
exit 0
