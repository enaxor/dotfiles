#!/usr/bin/env bash

echo $PATH | egrep -q "(^|:)/sbin($|:)" || export PATH="/sbin:$PATH"
echo $PATH | egrep -q "(^|:)/usr/sbin($|:)" || export PATH="/usr/sbin:$PATH"
echo $PATH | egrep -q "(^|:)/usr/local/sbin($|:)" || export PATH="/usr/local/sbin:$PATH"
if [ -n "$OPT_ANDROID_SDK_PATH" ]; then
    echo $PATH | egrep -q "(^|:)$OPT_ANDROID_SDK_PATH/tools($|:)" || export PATH="$OPT_ANDROID_SDK_PATH/tools:$PATH"
    echo $PATH | egrep -q "(^|:)$OPT_ANDROID_SDK_PATH/platform-tools($|:)" || export PATH="$OPT_ANDROID_SDK_PATH/platform-tools:$PATH"
fi
echo $PATH | egrep -q "(^|:)$HOME/bin($|:)" || export PATH="$HOME/bin:$PATH"

export JAVA_HOME=/opt/java_oracle/jdk1.8


function run {
    if ! pgrep $1 ;
    then
        $@&
    fi
}

# set keyboard layout
run "setxkbmap -layout gb"
# active numlock
run "numlockx on"

run "terminator"
run "mpd"
run "xbindkeys"
run "parcellite"
run "pidgin"
run "sylpheed"
run "chromium"
run "xscreensaver -no-splash"
run "ario"

run "conky -d -c $HOME/.config/conky/conky_left.conf"
run "conky -d -c $HOME/.config/conky/conky_right.conf"

