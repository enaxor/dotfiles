--[[
  sxtheme for Awesome WM 4.1
--]]

local os      = { getenv = os.getenv }
local theme_root_path = os.getenv("HOME") .. "/.config/awesome/themes/sxtheme/"
local theme_icons_path = os.getenv("HOME") .. "/.config/awesome/themes/sxtheme/icons/"

-- {{{ Main
local theme = {}
theme.wallpaper = theme_root_path .. "wallpaper.jpg"
-- }}}

-- {{{ Styles
theme.font      = "mono 9"

-- {{{ Colors
theme.fg_normal  = "#DCDCCC"
theme.fg_focus   = "#D7D7D7"
theme.fg_urgent  = "#00ffff"
theme.bg_normal  = "#151515"
theme.bg_focus   = "#540008"
theme.bg_urgent  = "#3F3F3F"
theme.bg_systray = theme.bg_normal
-- }}}

-- {{{ Borders
theme.useless_gap   = 0
theme.border_width  = 1
theme.border_normal = "#000000"
theme.border_focus  = "#000000"
theme.border_marked = "#000000"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#101010"
theme.titlebar_bg_normal = "#2F2F2F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = 15
theme.menu_width  = 100
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = theme_icons_path .. "taglist/squarefz.png"
theme.taglist_squares_unsel = theme_icons_path .. "taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = theme_icons_path .. "awesome-icon.png"
theme.menu_submenu_icon      = theme_icons_path .. "submenu.png"
-- custom icons
theme.battery = theme_icons_path .. "battery.png"
theme.cpu     = theme_icons_path .. "cpu.png"
theme.memory  = theme_icons_path .. "mem.png"
theme.swap    = theme_icons_path .. "swap.png"
theme.thermal = theme_icons_path .. "thermal.png"
theme.hdd     = theme_icons_path .. "hdd.png"
theme.note    = theme_icons_path .. "note.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = theme_icons_path .. "layouts/tile.png"
theme.layout_tileleft   = theme_icons_path .. "layouts/tileleft.png"
theme.layout_tilebottom = theme_icons_path .. "layouts/tilebottom.png"
theme.layout_tiletop    = theme_icons_path .. "layouts/tiletop.png"
theme.layout_fairv      = theme_icons_path .. "layouts/fairv.png"
theme.layout_fairh      = theme_icons_path .. "layouts/fairh.png"
theme.layout_spiral     = theme_icons_path .. "layouts/spiral.png"
theme.layout_dwindle    = theme_icons_path .. "layouts/dwindle.png"
theme.layout_max        = theme_icons_path .. "layouts/max.png"
theme.layout_magnifier  = theme_icons_path .. "layouts/magnifier.png"
theme.layout_floating   = theme_icons_path .. "layouts/floating.png"
-- }}}

-- {{{ Titlebar
-- close
theme.titlebar_close_button_focus  = theme_icons_path .. "titlebar/close_focus.png"
theme.titlebar_close_button_normal = theme_icons_path .. "titlebar/close_normal.png"
-- on top
theme.titlebar_ontop_button_focus_active  = theme_icons_path .. "titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = theme_icons_path .. "titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = theme_icons_path .. "titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = theme_icons_path .. "titlebar/ontop_normal_inactive.png"
-- sticky
theme.titlebar_sticky_button_focus_active  = theme_icons_path .. "titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = theme_icons_path .. "titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = theme_icons_path .. "titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = theme_icons_path .. "titlebar/sticky_normal_inactive.png"
-- floating
theme.titlebar_floating_button_focus_active  = theme_icons_path .. "titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = theme_icons_path .. "titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = theme_icons_path .. "titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = theme_icons_path .. "titlebar/floating_normal_inactive.png"
-- maximized
theme.titlebar_maximized_button_focus_active  = theme_icons_path .. "titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = theme_icons_path .. "titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = theme_icons_path .. "titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = theme_icons_path .. "titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}

return theme
